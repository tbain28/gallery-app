<?php 

    require( 'includes/config.inc.php' );
    require( 'includes/connect.inc.php' ); 


    if(isset( $_GET[ 'image_id' ] ) 
       and is_int( intval( $_GET[ 'image_id' ] ) ) ){
        
        $image_id = intval( $_GET[ 'image_id' ] );
        
        if( isset( $_POST[ 'tags' ] ) ){
            //the tag form was submitted
            
            //split the tags into an array using commas
            $tags = explode( ',', $_POST[ 'tags' ] );
            
            //clean up the individual tags, (remove extra whitespace and uppercase letters)
            $tags_clean = array();
            foreach( $tags as $tag ){
                array_push( $tags_clean, strtolower(trim( $tag ) ) );
            }
            
            //create an insert statement to put the tags in the database
            $stmt = $db->prepare('INSERT INTO gallery_tags(tag_name) 
                                    VALUES(?)');
            
            //associate the $tag variable with the ? in the statement above
            $stmt->bind_param( 's', $tag );
                        
            //attempt to insert the tag into the database
            foreach( $tags_clean as $tag ){
                $stmt->execute();                
            }
            $stmt->close();
            
            $tags_with_ids = array();
            
            $stmt = $db->prepare( 'SELECT id FROM gallery_tags
            WHERE tag_name = ?' );
            $stmt->bind_param( 's', $tag );
            $stmt->bind_result( $tag_id );
            
            foreach( $tags_clean as $tag ){
                $stmt->execute();
                $stmt->fetch();
                
                
                $tags_with_ids[ $tag_id ] = $tag;
            }
            
            //close to free up memory
            $stmt->close();
            
            //prepare statement to insert a new row, which will associate a tag with a row
            $stmt= $db->prepare('INSERT INTO gallery_images_tags(image_id, tag_id)
                                        VALUES(?,?)');
            
            // associate the image_id and tag as parameter for the query
            $stmt->bind_param( 'ii', $image_id, $tag_id);
            
            foreach( $tags_with_ids as $tag_id => $tag_name){
                $stmt->execute();
            }
            
        }
    }else{
        header ( 'Location: index.php' );
    }

?>
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Upload - Gallery App</title>
        <link rel="stylesheet" href="css/style.css" />
    </head>
    <body>
        <main class="wrapper" role="main">
         <header id="site-header">
            <h1><a href="index.php">Gallery App</a></h1>
        </header>
            <!-- the enctype attribute instructs the browser to 
                 break the file being uploaded into pieces for
                 transmission -->
            <h2>Add Tags</h2>
            <?php echo 'image id:' . $_GET[ 'image_id' ]; ?>
            <form action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" method="post">
                <ol>
                 
                    <li>
                       <label for="tags">Tags</label>
                       
                        <input type="text" name="tags" value="<?php if(is_array($tags_clean)){echo implode( ',', $tags_clean); } ?>" />
                    </li>
                    
                     <li>         
                        <!-- submit button  -->     
                        <input type="submit"
                               value="save" />
                    </li>
                </ol>
            </form>
        </main>
    </body>
</html>