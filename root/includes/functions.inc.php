<?php
    /**
    *Resizes an image proportionally to match a specific width
    *
    *@param string $filename the source file path to the image
    *@param string $destination_folder folder to which the image will be written
    *@param int $width the target widh of the finished image in pixels
    *
    *@return string the file path of the newly generated image
    */
    function resize_to_width( $filename, $destination_folder, $width ){
        
        // read the image into the web server's memory
            // figure out the type of image it is
        $dimensions = getimagesize( $filename );
        $type = $dimensions[ 'mime' ];
        
        switch( $type ){
            case 'image/png':
                $original = imagecreatefrompng( $filename );
            break;
            case 'image/jpeg':
            case 'image/pjpeg':
                $original = imagecreatefromjpeg( $filename );
            break;
            case 'image/gif':
                $original = imagecreatefromgif( $filename );
            break;
            default:
                // something went wrong
            break;
        }
        
        // determine the width and height of the image
        $original_width  = $dimensions[ 0 ];
        $original_height = $dimensions[ 1 ];
        
        // calculate the aspect ratio
        $aspect_ratio = $original_width / $original_height;
        
//        echo "ratio: $aspect_ratio, 
//              width: $original_width, 
//              height: $original_height, 
//              type: $type";
        
        // determine the new height and width of the image
        $height = ceil( $width / $aspect_ratio );
        
//        echo "<br><br>new width: $width, new height: $height";
        
        // create a new empty image in memory, with the new dimensions
        $new_image = imagecreatetruecolor( $width, $height );
        
        if(strcmp($type,'image/png') == 0){
            //preserve transparency in PNG
            
            //disable blending which makes all pixels opaque by default
            imagealphablending( $new_image, false);
            //create a new colour specific to our bew image, that will be used as the transparent colour
            $transparency = imagecolorallocatealpha( $new_image, 255, 255, 255, 127 );
            //fill image with the new color. which is transparent
            imagefill( $new_image, 0, 0, $transparent);
            //alow partial opacity, which is seen in PNGs but not GIFs and is off by defaul in PHP
            imagesavealpha($new_image, true);
            
        }
        // copy big image pixels into the new image, while
        // resampling the image data
        imagecopyresampled( $new_image, 
                            $original, 
                            0, 0, 0, 0, 
                            $width, $height, 
                            $original_width, $original_height );
        
        $destination_filename = $destination_folder 
                                . array_pop( explode( '/', $filename ) );
        
        // write the new image to the destination folder
        switch( $type ){
            case 'image/png':
                imagepng( $new_image, $destination_filename, floor( ( (10 - IMAGE_QUALITY) /10) * 9 ));
            break;
            case 'image/jpeg':
            case 'image/pjpeg':
                imagejpeg( $new_image, $destination_filename, IMAGE_QUALITY * 10 );
            break;
            case 'image/gif':
                imagegif( $new_image, $destination_filename );
            break;
            default:
                // something went wrong
            break;
        }
        
        // free up the image memory
        imagedestroy( $original );
        imagedestroy( $new_image );
        
        // return filename of the new image
        return $destination_filename;
    }