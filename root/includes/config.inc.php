<?php
define( 'UPLOADS_FOLDER', 'uploads/' );
define( 'THUMBS_FOLDER', UPLOADS_FOLDER.'thumbs/' );
define( 'ALLOWED_FILE_TYPES', 
        'image/jpeg,image/pjpeg,image/png,image/gif' );
define( 'RANDOMIZED_FILENAMES', true );
define ( 'IMAGE_QUALITY', 10 );
