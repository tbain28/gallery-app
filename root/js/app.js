$(document).ready(function(){
    console.log( "Started" ); 
    var $grid = $( '#gallery-list' ).masonry({
        columnWidth : 300,
        itemSelector: 'li',
        gutter : 20
        
    });
    
    $grid.imagesLoaded().progress(function(){
       $grid.masonry( 'layout' ); 
    });
});