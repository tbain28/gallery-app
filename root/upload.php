<?php

    // load the image functions
    require( 'includes/config.inc.php' );
    require( 'includes/functions.inc.php' );
    require( 'includes/connect.inc.php' );

    // the folder to which uploaded files will be copied

    $typeToExtension = array(
        'image/png' => '.png',
        'image/jpeg' => '.jpg',
        'image/pjpeg' => '.jpg',
        'image/gif' => '.gif'
    );
    
    $errors = array();

    if( isset( $_POST[ 'submitted' ] ) ){
        // form was submitted 
        
        if( strlen( $_FILES[ 'user-upload' ][ 'name' ] ) < 1 ){
            $errors[ 'file' ] 
                = '<p class="error">Please select a file to upload.</p>';
        } else {
            
            // check the file type
            if( strpos( ALLOWED_FILE_TYPES, 
                        $_FILES[ 'user-upload' ][ 'type' ] ) === false ){
                $errors[ 'type' ] 
                    = '<p class="error">The file type uploaded is not supported. Please upload a JPEG, PNG or GIF.</p>';
            } else {

                // try to get the dimensions of the image
                if( !getimagesize( $_FILES[ 'user-upload' ][ 'tmp_name' ] ) ){
                    $errors[ 'corrupted' ]
                        = '<p class="error">The file uploaded was corrupted
                                            or not an image.</p>';
                } else {

                    /* two-dimensional array
                       first 'key' selects the file input that was used
                       the second 'key' selects the piece of data that we
                       want to access about that file
                    */
                    $temporary_location = $_FILES[ 'user-upload' ][ 'tmp_name' ];
                    if(!RANDOMIZED_FILENAMES){
                        $destination = UPLOADS_FOLDER . $_FILES[ 'user-upload' ][ 'name' ];
                    }else{
                        $type = $_FILES[ 'user-upload' ][ 'type' ];
                        $extension = $typeToExtension[$type];
                        $filename = sha1(time(). rand(0, 10000) . $_SERVER[ 'REMOTE_ADDR' ]);
                        $destination = UPLOADS_FOLDER . $filename . $extension;
                    }

                    if( move_uploaded_file( $temporary_location, 
                                            $destination ) ){
                        
                        
                        $thumb = resize_to_width( $destination, 
                                         THUMBS_FOLDER,
                                         300 );
                        $preview 
                            = '<img src="' 
                              . $thumb 
                              . '" alt="Preview Image" />';
                        
                        //image has been uploaded and resized sucessfully
                        if(strlen ($_POST[ 'title' ])  < 1 ){
                            $errors[ 'title' ] = '<p class="errors"> Please enter a title </p>';
                            //delete files, since there was an error
                            unlink($thumb);
                            unlink($destination);
                            $preview = '';
                        }else{
                            $title = strip_tags($_POST[ 'title' ]);
                            
                            $descrition = strip_tags($_POST[ 'description' ]);         
                            
                            $image_filename = array_pop(explode( '/', $destination));
                            
                            /*$query = "INSERT INTO
                                        gallery_images(title,
                                                       description,
                                                       image_filename)
                                        VALUES('$title', '$descrition', '$image_filename')";                        
                            $result = mysqli_query($db, $query) or die( mysqli_error($db));*/
                            
                            $stmt = $db->prepare( 'INSERT INTO
                                                   gallery_images(title,
                                                   description,
                                                   image_filename) VALUES(?,?,?)' );
                            $stmt->bind_param( 'sss', $title, $description, $image_filename );
                            $stmt->execute();
                            
                            $_POST[ 'title'] ='' ;
                            $_POST[ 'description' ] = ''; 
                            
                        }
                        
                    } else {
                        $preview = '<p>Error moving file, 
                                       no preview available.</p>';
                    }
                }
            }
        }
    }
?>
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Upload - Gallery App</title>
        <link rel="stylesheet" href="css/style.css" />
    </head>
    <body>
        <main class="wrapper" role="main">
         <header id="site-header">
            <h1><a href="index.php">Gallery App</a></h1>
        </header>
            <!-- the enctype attribute instructs the browser to 
                 break the file being uploaded into pieces for
                 transmission -->
            <h2>Upload</h2>
            <?php
                echo $errors[ 'file' ];
                echo $errors[ 'type' ];
                echo $errors[ 'corrupted' ];
                
                echo $preview;
            ?>
            
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post"
                  enctype="multipart/form-data">
                <ol>
                        <!-- title of the post -->
                        <li>
                           <?php echo $errors[ 'title' ]; ?>
                            <label for="title">Title</label>
                            <input name="title" type="text" size="80" maxlength="255" value="<?php $_POST['title']; ?>">                            
                        </li>
                        
                        
                        <!-- description of image being posted-->
                        <li>
                            <label for="description">Description</label>
                            <textarea name="description" rows="6" cols="80" ><?php $_POST['description']; ?></textarea>
                        </li>
                    <li>
                        <!-- extra input so we can detect form submissions -->
                        <input type="hidden" name="submitted" value="true" />
                        
                        <!-- file browse form control -->
                        <div class="file-browse-conainer">
                        <input type="file" 
                               name="user-upload" />
                               <span class="file-browse-button">Browse</span>
                        </div>
                               
                        <!-- submit button  -->     
                        <input type="submit"
                               value="Upload" />
                    </li>
                </ol>
            </form>
        </main>
    </body>
</html>