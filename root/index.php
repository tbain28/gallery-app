<?php 

    require( 'includes/config.inc.php' );
    require( 'includes/connect.inc.php' ); 
    /*
    
    Procedural:
    -----------------------
    $query = 'SELECT id,image_filename, title, description FROM gallery_images ORDER BY id DESC';
    $result = mysqli_query( $db, $query)
        or die(mysqli_error($db));
    
    MySQLi OOP:
    -----------------------
    $result = $db->query( 'SELECT id,image_filename, title, description FROM gallery_images ORDER BY id DESC' );
    */

    if( isset( $_GET[ 'tag_id' ])
        and is_int( intval($_GET[ 'tag_id' ]))){
        //only grab images with the tag id that is in the url
        $stmt = $db->prepare( 'SELECT gallery_images.id, gallery_images.image_filename, gallery_images.title, gallery_images.description 
        
                               FROM gallery_images                               
                               INNER JOIN gallery_images_tags
                               
                               ON gallery_images.id
                                 = gallery_images_tags.image_id
                                
                               WHERE gallery_images_tags.tag_id = ?
                               
                               ORDER BY id DESC' );
        
        $selected_tag_id = intval( $_GET[ 'tag_id' ]);
        $stmt->bind_param( 'i', $selected_tag_id);
    }else{  
        $stmt = $db->prepare( 'SELECT id, image_filename, title, description FROM gallery_images ORDER BY id DESC' );
    }
    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result( $id, $image_filename, $title, $desc );

    $tag_stmt = $db->prepare( 'SELECT gallery_images_tags.tag_id, 
                                      gallery_tags.tag_name
    
                                FROM gallery_images_tags 
                                INNER JOIN gallery_tags 
                                
                                ON gallery_images_tags.tag_id 
                                  = gallery_tags.id
                                
                                WHERE image_id = ?');

    $tag_stmt->bind_param( 'i', $id );
    $tag_stmt->bind_result( $tag_id, $tag_name );
    
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Gallery App</title>
    <link rel="stylesheet" href="css/style.css" />
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.2/masonry.pkgd.min.js"></script>-->
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/app.js"></script>
</head>

<body>
    
    <main class="wrapper" role="main">
        <header id="site-header">
            <h1><a href="index.php">Gallery App</a></h1>
        </header>
        
        <ul id="gallery-list">
            <?php while( $stmt->fetch() ): ?>
            <li>
                    <figure>
                       <a href="<?php echo UPLOADS_FOLDER . $image_filename; ?>" target="_blank">
                        <img src="<?php echo THUMBS_FOLDER . $image_filename; ?>" alt="<?php echo $title; ?>" />
                        </a>
                        <figcaption>
                            <h3><?php echo $title; ?></h3>
                            <p><?php echo $desc; ?></p>
                            <aside>
                                <?php
                                    $tag_stmt->execute();
                                    while( $tag_stmt->fetch() ){
                                        ?>
                                <a href="index.php?tag_id=<?php echo $tag_id ?>"><?php echo $tag_name. ' '; }?> </a>
                            </aside>
                            <nav>
                                <a href="add-tags.php?image_id=<?php echo $id ?>">Add Tags</a>
                            </nav>
                        </figcaption>
                    </figure>
            </li>
            <?php endwhile; ?>
        </ul>
    </main>
  
</body>
</html>
